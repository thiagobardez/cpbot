var Twit = require('twit');

// Bot
var Bot = module.exports = function(config) {
    this.twit = new Twit(config);
};


Bot.prototype.tweet = function(status, callback) {
    if (typeof status !== 'string') {
        return callback(new Error('tweet must be of type String'));
    } else if (status.length > 140) {
        return callback(new Error('tweet is too long: ' + status.length));
    }
    this.twit.post('statuses/update', {
        "status": status
    }, callback);
};


Bot.prototype.retweet = function(id, callback) {
    this.twit.post('statuses/retweet/:id', {
        "id": id
    }, callback);
};

Bot.prototype.search = function(keywords, user, callback) {

    this.twit.get('statuses/user_timeline', {
        screen_name: user
    }, function(err, data, response) {

        if (err) {
            console.log(err)
            return
        }

        // itera sobre todos os últimos tweets do usuário
        for (var i = 0; i < data.length; i++) {

            // busca as keywords
            if ((data[i].text.match(new RegExp(keywords.join('|'))))
                // && (data[i].text.match(new RegExp("cpbr8")))
                && !(data[i].text.match(new RegExp("@")))) {

                // filtra se o tweet não é resposta/retweet de algum outro
                if (!data[i].in_reply_to_status_id && !data[i].in_reply_to_user_id && !data[i].in_reply_to_screen_name) {

                    callback(data[i].id_str);
                }

            }
        }

    });

};