var Bot = require('./bot');
var http = require('http');

var config = {
	consumer_key: 'CeZH8Xx4ofoECoFzooDMmdRzK',
	consumer_secret: 'mzniEb9bPRQLcxXduA2HFoULNWW87Tzz9hn116exeWDcIXiuE6',
	access_token: '3015908405-WZVdYh9swgPlOnl7ICLbUB1hWED47fNoSb8lqzJ',
	access_token_secret: '8sbOQmqHHQiYRAvZ3TJ4Qxk5e1EFrW9HR0yQrDNxDDOJT'
}

var bot = new Bot(config);

var palavras = [
	'aqui',

	'brinde',
	'brindes',
	'bancada',

	'chegue',
	'cheguem',
	'chega',
	'chegar',
	'chegarem',
	'corra',
	'corram',
	'concorrer',
	'concorra',

	'distribuição',
	'distribuindo',

	'ganha',
	'ganham',
	'ganhar',
	'garantir',

	'hackathon',

	'inscrever',
	'inscrição',

	'promoção',
	'promoções',
	'presente',
	'presentinho',

	'quer',
	'que tal',

	'sorteio',
	'sorteios',
	'sortearemos',

	'valem',
	'vale',
	'vem',
	'vier',
	'vir',
	'venha',
	'venham'
];

var users = [
	'syfycpbr',
	'factory_geek',
	'PayPal_BR',
	'br_paypaldev',
	'3coracoes',
	'submarino',
	'sebrae',
	'comunic',
	'cwebbr',
	'outbackbrasil',
	'tvguniversidade',
	'yourvoicemobi'
];

var index = 0;
var cache = [];


function search() {
	user = users[index];
	// console.log("Buscando informações de: " + user)

	bot.search(palavras, user, function(id) {
		if (!cache[id]) {
			// console.log("Retweetando: " + id);

			bot.retweet(id, function(err, data, response) {
				cache[id] = true;

				if (err) {
					console.log(err);
				}
				// adiciona o id no cache
				else {
					// console.log("Ok.");
				}
			})
		}
	});

	index = index + 1 < users.length ? index + 1 : 0;
}

setInterval(search, 5010);

var server_port = process.env.YOUR_PORT || process.env.PORT || 5000;

// Configure our HTTP server to respond with Hello World to all requests.
var server = http.createServer(function(request, response) {
	response.writeHead(200, {
		"Content-Type": "text/plain"
	});
});

// Listen on port 8000, IP defaults to 127.0.0.1
server.listen(server_port);

// Put a friendly message on the terminal
console.log("Server running at %d", server_port);